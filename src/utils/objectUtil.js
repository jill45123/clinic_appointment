
const generateSingletonInstance = (symbol, constFunc) => {

    if (Object.keys(global).findIndex(element => element === symbol) === -1) {
        global[symbol] = constFunc();
    }

    return global[symbol];
}

export { generateSingletonInstance };