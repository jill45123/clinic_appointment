
const patients = (sequelize, DataTypes) => {
    const Patients = sequelize.define('patients', {
        patient_id: {
            allowNull: false, 
            primaryKey: true, 
            type: DataTypes.INTEGER,
            autoIncrement: true, 
        },
        patient_name: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
        identity: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
        patient_sex: {
            allowNull: false,
            type: DataTypes.STRING(1),
        },
        patient_birth: {
            allowNull: false,
            type: DataTypes.DATE
        },
        blood_type: {
            allowNull: false,
            type: DataTypes.STRING(2),
        },
        patient_contact: {
            allowNull: false,
            type: DataTypes.STRING(15),
        },
        patient_address: {
            allowNull: false,
            type: DataTypes.STRING(30),
        },
        is_allergy: {
            allowNull: false,
            type: DataTypes.BOOLEAN
        },
    }, {
        timestamps: false,
    });

    return Patients;
}

export { patients }