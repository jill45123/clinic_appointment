
const admins = (sequelize, DataTypes) => {
    const Admins = sequelize.define('admins', {
        admin_id: {
            allowNull: false, 
            primaryKey: true, 
            type: DataTypes.INTEGER,
            autoIncrement: true, 
        },
        account: {
            allowNull: false,
            type: DataTypes.STRING(20),
        },
        password: {
            allowNull: false,
            type: DataTypes.STRING,
        },
        admin_name: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
    }, {
        timestamps: false,
    });

    return Admins;
}

export { admins }