
const departments  = (sequelize, DataTypes) => {
    const Departments  = sequelize.define('departments', {
        department_id: {
            allowNull: false, 
            primaryKey: true, 
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
        },
        type_name: {
            allowNull: false,
            type: DataTypes.STRING(20),
        },
        department_name: {
            allowNull: false,
            type: DataTypes.STRING(20),
        },
    }, {
        timestamps: true,
    });
    return Departments;
}

export { departments }