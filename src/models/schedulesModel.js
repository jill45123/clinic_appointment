
const schedules  = (sequelize, DataTypes) => {
    const Schedules  = sequelize.define('schedules', {
        schedules_id: {
            allowNull: false, 
            primaryKey: true, 
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
        },
        doctor_id: {
            allowNull: false,
            type: DataTypes.UUID,
        },
        schedule_week: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
        schedule_period: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
    }, {
        timestamps: true,
    });
    return Schedules;
}

export { schedules }