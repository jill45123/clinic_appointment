
const reserves  = (sequelize, DataTypes) => {
    const Reserves  = sequelize.define('reserves', {
        reserve_id: {
            allowNull: false, 
            primaryKey: true, 
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
        },
        patient_id: {
            allowNull: false,
            type: DataTypes.UUID,
        },
        doctor_id: {
            allowNull: false,
            type: DataTypes.UUID,
        },
        reserve_date: {
            allowNull: false,
            type: DataTypes.DATE,
        },
        reserve_period: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
    }, {
        timestamps: true,
    });
    return Reserves;
}

export { reserves }