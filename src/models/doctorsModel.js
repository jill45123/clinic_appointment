
const doctors  = (sequelize, DataTypes) => {
    const Doctors  = sequelize.define('doctors', {
        doctor_id: {
            allowNull: false, 
            primaryKey: true, 
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
        },
        doctor_name: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
        doctor_about: {
            allowNull: false,
            type: DataTypes.STRING(200),
        },
        doctor_clinic: {
            allowNull: false,
            type: DataTypes.STRING(10),
        },
        department_id: {
            allowNull: false,
            type: DataTypes.UUID,
        },
    }, {
        timestamps: true,
    });
    Doctors.associate = (models) => {
        const { schedules,department } = models;
        Doctors.hasOne(schedules, {
          foreignKey: 'doctor_id',
        });
      };
    return Doctors;
}

export { doctors }