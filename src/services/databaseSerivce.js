import Sequelize from 'sequelize'; 

import dbConfig from '../config/database';
import { generateSingletonInstance } from '../utils/objectUtil';

class databaseService {
    connect = () => {
        const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, {
            host: dbConfig.host, 
            dialect: dbConfig.connection 
        });

        return sequelize;
    }
}

export default generateSingletonInstance('SERVICES_DATABASESERVICE', () => new databaseService());