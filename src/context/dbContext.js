import { Sequelize, DataTypes } from 'sequelize';
import databaseService from '../services/databaseSerivce';

import { admins } from '../models/adminsModel';
import { schedules } from '../models/schedulesModel';
import { doctors } from '../models/doctorsModel';
import { departments } from '../models/departmentsModel';
import { patients } from '../models/patientsModel';
import { reserves } from '../models/reservesModel';
class Database {
    constructor() {
        const db = {};
        const sequelize = databaseService.connect();

        db.Sequelize = Sequelize;
        db.sequelize = sequelize;

        db.Models = {};
        db.Models.admins = admins(sequelize, DataTypes);
        db.Models.doctors = doctors(sequelize, DataTypes);
        db.Models.schedules = schedules(sequelize, DataTypes);
        db.Models.doctors=doctors(sequelize,DataTypes);
        db.Models.departments=departments(sequelize,DataTypes);
        db.Models.patients=patients(sequelize,DataTypes);
        db.Models.reserves=reserves(sequelize,DataTypes);

        return db;
    }
}

export default new Database();