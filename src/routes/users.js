import { Router } from 'express';
import UsersController from '../http/controller/usersController'

const router = Router();

const usersController = new UsersController();

router.get('/', usersController.Get);
router.post('/login', usersController.Login);
router.post('/', usersController.Post);
router.patch('/', usersController.Patch);
router.delete('/', usersController.Delete);



export default router;