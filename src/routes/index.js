import { Router } from 'express';
import user from './users';

const router = Router();

router.use('/user', user);

export default router;