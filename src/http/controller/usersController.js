// .src/http/controllers/UsersController.js
import UsersServices from '../services/usersServices';

const usersServices = new UsersServices();

class UsersController {
    Get = async (req, res) => {
        if(!req.header('Authorization')){
            return res.status(401).json({status: 401, message: '尚未登入'});
        }
        res.setHeader('Content-Type', 'application/json');
        const response = await usersServices.Get(req);
        if (response.status === 200)
            res.status(200).json(response);
        else
            res.status(400).json(response);
    }
    Post = async (req,res)=>{
        if(!req.header('Authorization')){
            return res.status(401).json({status: 401, message: '尚未登入'});
        }
        res.setHeader('Content-Type', 'application/json');
        const response = await usersServices.Post(req);
        if (response.status === 201)
            res.status(201).json(response);
        else
            res.status(400).json(response);
    }
    Login = async (req,res)=>{
        res.setHeader('Content-Type', 'application/json');
        const response = await usersServices.Login(req);
        if (response.status === 200)
            res.status(200).json(response);
        else
            res.status(400).json(response);
    }
    Patch = async (req,res)=>{
        if(!req.header('Authorization')){
            return res.status(401).json({status: 401, message: '尚未登入'});
        }
        res.setHeader('Content-Type', 'application/json');
        const response = await usersServices.Patch(req);
        if (response.status === 200)
            res.status(200).json(response);
        else
            res.status(400).json(response);
    }
    Delete = async (req,res)=>{
        if(!req.header('Authorization')){
            return res.status(401).json({status: 401, message: '尚未登入'});
        }
        res.setHeader('Content-Type', 'application/json');
        const response = await usersServices.Delete(req);
        if (response.status === 200)
            res.status(200).json(response);
        else
            res.status(400).json(response);
    }
}

export default UsersController;