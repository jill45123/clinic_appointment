import dbContext from '../../context/dbContext';
import sha256 from 'sha256';
import jwt from 'jsonwebtoken'
import { use } from 'chai';
const { users } = dbContext.Models;
const { schedules } = dbContext.Models;


class UsersServices {
    Get = async (req) => {
        try {
            const usersResponse = await users.findAll();

            return ({ status: 200, message: 'success', data: usersResponse });
        } catch (error) {
            return ({ status: 400, message: error.message });
        }
    }

    Post = async (req) => {
        try {
            const { body } = req;
            await schedules.create({
                ...body
            });
            return ({ status: 201, message: 'success' });
        } catch (error) {
            return ({ status: 400, message: error.message });
        }
    }

    Login = async (req) => {
        try{
            const { account, password } = req.body;
            const userCheck = await users.findOne({
                where:{
                    account:account,
                    password:sha256(password)
                }
            });
            if(!userCheck){
                return ({ status: 400, message: '帳號或密碼輸入錯誤' });
            }
            const payload ={
                user_id: userCheck.user_id,
                account: userCheck.account,
            }
            console.log(payload)
            const token = jwt.sign({ payload, exp: Math.floor(Date.now() / 1000) + (60 * 15) }, 'my_secret_key');
            return ({ status: 200, message: 'success',token });
        }
        catch(error){
            return ({ status: 400, message: error.message });
        }
    }

    Patch = async (req) => {
        try {
            const { account, oldPassword, newPassword} = req.body;
            const usersResponse = await users.findOne({
               where:{
                   account:account,
                   password:sha256(oldPassword)
                }
            });
            
            if(!usersResponse){
                return ({ status: 400, message: '帳號或密碼輸入錯誤' });
            }
            const password=sha256(newPassword);
            await users.update({
                password
                 },{
                where: {
                    user_id: usersResponse.user_id
                }
            });

            return ({ status: 200, message: 'success' });
        } catch (error) {
            return ({ status: 400, message: error.message });
        }
    }

    Delete = async (req) => {
        try {
            const { user_id } = req.body;
            const userCheck = await users.findOne({
                where:{
                    user_id:user_id
                }
            });
            if(!userCheck){
                return ({ status: 400, message: '此Id不存在' });
            }
            await users.destroy({
                where:{
                    user_id:user_id
                }
            });
            return ({ status: 200, message: '刪除成功' });
        } catch (error) {
            return ({ status: 400, message: error.message });
        }
    }
}

export default UsersServices;